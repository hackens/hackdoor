#include <iostream>
#include <fstream>
#include <string>
#include <sstream>
#include <unistd.h>
#include <time.h>
#include <sys/time.h>

#define RPIN 39     //13
#define GPIN 38     //12
#define BPIN 25     //11
#define SPIN 3      //PWM3
#define MPIN1 27    //7

#define MR_STOP1 24  //6
#define MR_STOP2 27  //7
#define MR_BOLT1 17  //5 Above the bolt
#define MR_BOLT2 28  //4 left of the bolt

#define SVAL_CW 2*9       //TESTME
#define SVAL_CCW 2*18     //TESTME
#define SVAL_BLOCK 2*12   //TESTME
#define SVAL_ZERO 0     //TESTME

#define TIMEOUT 10000    //TESTME

//#define DEBUG
//#define MANUAL_COMMAND


using namespace std;
std::string status="unknown";


/** UTILITY **/

int time() {
    struct timeval start, end;

    long mtime, seconds, useconds;

    gettimeofday(&start, NULL);

    seconds  = start.tv_sec;
    useconds = start.tv_usec;

    mtime = ((seconds) * 1000 + useconds/1000.0) + 0.5;
    return mtime;
}

/** LEVEL 0 **/

int writefile(std::string file, int value) {
    ofstream f;
    f.open(file.c_str());
    if (!f.is_open()) return -1;
    f << value;
    f.close();
    return 0;
}

int writefile(std::string file, std::string value) {
    ofstream f;
    f.open(file.c_str());
    if (!f.is_open()) return -1;
    f << value;
    f.close();
    return 0;
}

int readfile(std::string file) {
  ifstream f;
  string line;
  f.open(file.c_str());
  if (!f.is_open()) return -1;
  getline (f,line);
  if (line.length()!=1) return -2;
  if (line[0]=='0') return 0;
  if (line[0]=='1') return 1;
  return -3;
}

std::string string_of_int(int v) {
    ostringstream convert;
    convert << v;
    return convert.str();
}

int led_init(int pin) {
  std::cout<<"led_init "<<pin<<std::endl;
  int a = writefile("/sys/class/gpio/export",pin);
  int b = writefile("/sys/class/gpio/gpio"+string_of_int(pin)+"/direction","out");
  return a*2+b;
}

int led_value(int pin, int value) {
  //std::cout<<"led_value "<<pin<<","<<value<<std::endl;
  return writefile(("/sys/class/gpio/gpio"+string_of_int(pin))+"/value",1-value);
}

int servo_init(int pin) {
  std::cout<<"servo_init "<<pin<<std::endl;
  int a=writefile("/sys/class/pwm/pwmchip0/export",pin);
  int b=writefile("/sys/class/pwm/pwmchip0/pwm"+string_of_int(pin)+"/enable",1);
  int c=writefile("/sys/class/pwm/pwmchip0/pwm"+string_of_int(pin)+"/period",1000000);
  return 4*a+2*b+c;
}

int servo_value(int pin, int percent) {
  std::cout<<"servo_value "<<pin<<","<<percent<<std::endl;
  return writefile("/sys/class/pwm/pwmchip0/pwm"+string_of_int(pin)+"/duty_cycle",percent*10000);
}

int microrupt_init(int pin) {
  std::cout<<"microrupt_init "<<pin<<std::endl;
  int a=writefile("/sys/class/gpio/export",pin);
  int b=writefile("/sys/class/gpio/gpio"+string_of_int(pin)+"/direction","in");
  return 2*a+b;
}

int microrupt_value(int pin) {
  return readfile("/sys/class/gpio/gpio"+string_of_int(pin)+"/value");
}


/** LEVEL 1 **/

int led_is_green=0;
int led_is_red=0;

int init() {
  led_init(RPIN);
  led_init(GPIN);
  led_init(BPIN);
  servo_init(SPIN);
  microrupt_init(MR_BOLT1);
  microrupt_init(MR_BOLT2);
  microrupt_init(MR_STOP1);
  microrupt_init(MR_STOP2);
}

int led_off() {
    led_is_green=0;
    led_is_red=0;
    led_value(RPIN,0);
    led_value(GPIN,0);
    led_value(BPIN,0);
}

int led_red() {
    if (led_is_red) return -1;
    led_off();
    led_value(RPIN,1);
    led_is_red=true;
}

int led_flash_green() {
    led_off();
    led_value(GPIN,1);
    usleep(100000);
    led_red();
}

int led_blue() {
    led_off();
    led_value(BPIN,1);
}

int led_white() {
    led_off();
    led_value(GPIN,1);
    led_value(RPIN,1);
    led_value(BPIN,1);
}

int led_green() {
    if (led_is_green) return -1;
    led_off();
    led_value(GPIN,1);
    led_is_green=1;
}

/* timeout in millisecs*/
int wait_on_mr(int pin, int timeout, bool fallingEdge=false) {
#ifndef DEBUG
  int t=time();
  while (microrupt_value(pin) xor (not fallingEdge)) {
    if (time()-t>timeout) {
      return -1;
    }
  }
  return 0;
#else
  std::cout<<"Waiting on "<<(fallingEdge?"falling":"rising")<<" edge on MR "<<pin<<" : "<<std::endl;
  usleep(1000000);
#endif
}

int open_lock() {
  servo_value(SPIN,SVAL_CW);
  wait_on_mr(MR_BOLT2, TIMEOUT, false);
  servo_value(SPIN,SVAL_ZERO);
  return 0;
}

int close_lock() {
  servo_value(SPIN,SVAL_CCW);
  wait_on_mr(MR_BOLT1, TIMEOUT, true);
  servo_value(SPIN,SVAL_ZERO);
  return 0;
}

std::string poll_lock_status() {
#ifndef DEBUG
  int bolt1 = microrupt_value(MR_BOLT1);
  int bolt2 = microrupt_value(MR_BOLT2);
  if (bolt1 && bolt2) return "open";
  if (bolt1 && !bolt2) return "closed"; //"sigle turn or halfway";
  if (!bolt1 && bolt2) return "wtf ?";
  if (!bolt1 && !bolt2) return "closed";
#else
  return (microrupt_value(17)?"closed":"open");
  /* KBD mode
  std::string s;
  std::cout << "Enter lock status : ";
  std::cin >> s;
  return s;*/
#endif
}

std::string lock_status() {
  return poll_lock_status();
  /*
  if (status=="unknown") status=poll_lock_status();
  return status;*/
}

int error(std::string d) {
  std::cerr << d << std::endl;
  led_off();
  usleep(100000);
  for (int i=1; i<=5; i++) {
    led_red();
    usleep(50000);
    led_off();
    usleep(50000);
  }
}
 
// Returns 1 for open and 2 for close
int wait_for_button_or_nfc() {
#ifndef MANUAL_COMMAND
  int i=0;
  while (true) {
    usleep(100000);
    if (i % 20==0) led_off();
    else {
      if (readfile("openme.txt")==1) {writefile("openme.txt",0); return 1;}
      if (readfile("closeme.txt")==1) {writefile("closeme.txt",0); return 2;}
      if (readfile("denied.txt")==1) {writefile("denied.txt",0); error("Access denied");}

      status = poll_lock_status();
      if (status=="open") led_green();
      else if (status=="closed") led_red();
    }
    i++;
  }
#else
  std::string s;
  do {
    std::cout<<"Waiting for button input. Enter o or c : ";
    std::cin >> s;
  } while (!(s=="o" || s=="c"));
  if (s=="o") return 1;
  else return 2;
#endif
}

int set_lock_status(std::string expectedStatus) {
  if (expectedStatus==poll_lock_status()) {
    status=expectedStatus;
  }
  else {
    status=expectedStatus;
    error("AAAAAAHHHHHHHH !! Status is "+poll_lock_status());
  }
}

std::string door_status() {
#ifndef DEBUG
  int stop1 = microrupt_value(MR_STOP1);
  int stop2 = microrupt_value(MR_STOP2);
  if (stop1 && stop2) return "closed";
  if (!stop1 && stop2) return "inconsistent";
  if (stop1 && !stop2) return "inconsistent";
  if (!stop1 && !stop2) return "open";
#else
  std::string s;
  std::cout << "Enter door status : ";
  std::cin >> s;
  return s;
#endif
}


//TODO logic

int mainloop() {
  while (true) {
    if (lock_status()=="closed") led_red(); else led_green();
    int action = wait_for_button_or_nfc();
    if (action==1) {  //open
      if (lock_status()!="closed") error("Lock already opened");
      else {
        led_white();
        open_lock();
        //TODO errors
        led_green();
        set_lock_status("open");
      }
    }
    else if (action==2) { //close
      if (lock_status()!="open") error("Lock already closed");
      else {
        if (door_status() != "closed") error("Door is not closed !");
        else {
          led_white();
          close_lock();
          //TODO errors
          led_red();
          set_lock_status("closed");
        }
      }
    }
  }
}

int main() {
  FILE *fpipe;
  std::string command="./daemon_rfid";

  if ( !(fpipe = (FILE*)popen(command.c_str(),"r")) )
  {  // If fpipe is NULL
     perror("Problems with pipe");
  }

  init();
  led_off();
  servo_value(SPIN,SVAL_CW);
  sleep(1);
  servo_value(SPIN,SVAL_CCW);
  sleep(1);
  servo_value(SPIN,SVAL_ZERO);
  mainloop();
  sleep(1);
  return 0;
}

