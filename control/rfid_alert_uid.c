/**
 * @file alert_uid.c
 * @brief Alerts when a card is detected and gives the uid
 * @args You get DISPVERBOSE if argc>1
 * @state Return the uid after polling for max 20 secs
 * @todo  Decide what to do when we get we get an uid
 */

#include <stdlib.h>
#include <stdint.h>
#include <inttypes.h>
#include <signal.h>
#include <time.h>
#include <sys/time.h>
#include <unistd.h>
#include <nfc/nfc.h>

#define DISPVERBOSE(_msg)          do{ if(verbose) {fputs  ("" _msg "\n", stderr);       } }while(0);
#define DISPVERBOSEARG(_msg,_args) do{ if(verbose) {fprintf(stderr, "" _msg "\n", _args);} }while(0);
#define MIN(a,b) ((a < b) ? a : b)

static nfc_device *pnd = NULL;
static nfc_context *context = NULL;
static int verbose = 0;

static void print_hex(const uint8_t *pbtData, const size_t szBytes)
{
  size_t szPos;
  for (szPos = 0; szPos < szBytes; szPos++) {
    printf("%02x", pbtData[szPos]);
  }
}

/* Tries to put the RFID module back in an usable state so we do not get an error next time we try to use it.
   This error is quite random and can still appears even with this cleanup. */
static void stop_polling(__attribute__((unused)) int sig)
{
	DISPVERBOSE("SIGINT Received");
	if (pnd != NULL)
	{
		nfc_abort_command(pnd);
		nfc_close(pnd);
	}
	if(context != NULL)
	{
		nfc_exit(context);
	}
	fputs("\n", stderr);
	exit(EXIT_FAILURE);
}

/**
 * Wait for NFC card and return ID.
 * Return values:
 *   0: no card found
 *  between 1 and 2^32-1: card found; returns the card ID
 *  >=2^32: error (currently, only ~0 used)
 */
static uint64_t get_nfc_id(void)
{
	nfc_target nt;
	uint64_t id = ~UINT64_C(0);
	DISPVERBOSE("get_nfc_id() started");

	const uint8_t uiPollNr = 1;
	const uint8_t uiPeriod = 1; /* We might need to ++ this if the card is too far */
	const nfc_modulation nmModulations[1] = /* Hoping it is not an oversimplification, but the daemom example does it too */
    { { .nmt = NMT_ISO14443A, .nbr = NBR_106 } };
	const size_t szModulations = 1;

	if (nfc_initiator_poll_target(pnd, nmModulations, szModulations, uiPollNr, uiPeriod, &nt) < 0)
	{
		id = UINT64_C(0);
		DISPVERBOSE("No tag found");
		goto _end;
	}

	if (nt.nm.nmt == nmModulations[0].nmt && nt.nm.nbr == nmModulations[0].nbr)
	{
		if(verbose)
		{
			fputs("The following tag was found: ATQA (SENS_RES): ", stdout);
			print_hex(nt.nti.nai.abtAtqa, 2);
			printf(", UID (NFCID%c): ", (nt.nti.nai.abtUid[0] == 0x08 ? '3' : '1'));
			print_hex(nt.nti.nai.abtUid, nt.nti.nai.szUidLen);
			printf(", SAK (SEL_RES): ");
			print_hex(&nt.nti.nai.btSak, 1);
			printf("\n");
		}
		if((nt.nti.nai.szUidLen != 4) || (nt.nti.nai.abtAtqa[0] != 0x00) || (nt.nti.nai.abtAtqa[1] != 0x04) || (nt.nti.nai.btSak != 0x08) )
		{
			DISPVERBOSE("Error: Not a NormaleCarte"); /* Actually: not a Mifare 1K card */
			goto _error;
		}
		/* Assumes nt.nti.nai.szUidLen == 4 from above */
		id  = ((uint64_t)(nt.nti.nai.abtUid[3])) <<  0;
		id |= ((uint64_t)(nt.nti.nai.abtUid[2])) <<  8;
		id |= ((uint64_t)(nt.nti.nai.abtUid[1])) << 16;
		id |= ((uint64_t)(nt.nti.nai.abtUid[0])) << 24;
	} else
	{
		DISPVERBOSE("Unsupported NFC type.");
		goto _error;
	}

	_end:
	DISPVERBOSE("get_id() OK")
	return id;

	_error:
	DISPVERBOSE("get_id() failed")
	return ~UINT64_C(0);
}

int main(int argc, __attribute__((unused)) const char *argv[])
{
 	if(argc>1)
 	{
 		verbose = 1;
 	}
    signal(SIGINT, stop_polling);

	if(!system(NULL))
	{
		DISPVERBOSE("Error: no command processor")
		goto _error;
	}

	nfc_init(&context);	/* Initialize libnfc and set the nfc_context */
	if (context == NULL)
	{
		DISPVERBOSE("Error: Unable to init libnfc (malloc)");
		goto _error;
	}
	DISPVERBOSE("nfc_init() done")

	// Open, using the first available NFC device which can be in order of selection:
	//   - default device specified using environment variable or
	//   - first specified device in libnfc.conf (/etc/nfc) or
	//   - first specified device in device-configuration directory (/etc/nfc/devices.d) or
	//   - first auto-detected (if feature is not disabled in libnfc.conf) device
	pnd = nfc_open(context, NULL);
	if (pnd == NULL)
	{
		DISPVERBOSE("Error: Unable to open NFC device.");
		goto _error;
	}
	DISPVERBOSE("nfc_open() done")

	/* Set opened NFC device to initiator mode*/
	if (nfc_initiator_init(pnd) < 0)
	{
	   DISPVERBOSE("nfc_initiator_init() failed")  /* nfc_perror(pnd, "nfc_initiator_init"); */
		goto _error;
	}
	DISPVERBOSEARG("NFC reader: %s opened\n", nfc_device_get_name(pnd));


	uint64_t id = UINT64_C(0);
	//for(int i=0; i<50; i++)
    while(1)
	{
	 	uint64_t new_id;
	 	new_id = get_nfc_id();
	 	if(new_id > UINT64_C(0xFFFFFFFF))
	 	{
	 		puts("Failed to get ID");
	 		continue; /* An error happened. Let's try again. */
	 	}
	 	if(new_id != id)
	 	{
	 		int tryopen = 0;
	 		if(id == UINT64_C(0))
	 		{
	 			printf("New card found: %"PRIX64 "\n", new_id);
	 			tryopen = 1;
	 		} else if (new_id == UINT64_C(0))
	 		{
	 			printf("Card removed: %"PRIX64 "\n", new_id);
	 		} else
	 		{
	 			printf("New card found (but the previous one was not removed): %"PRIX64 "\n", new_id);
				tryopen = 1;
	 		}
	 		id = new_id;
	 		if(tryopen)
	 		{
	 			char command [100];
	 			snprintf(command, 24, "./auth.sh %"PRIX64, id);
	 			if(system(command))
	 			{
	 				DISPVERBOSE("Auth failed");
	 			}
	 		}
	 	} else
	 	{
	 		//TODO: Add timeout for removal
	 		//TODO: sleep here a little? (but detection is already quite slow...)
	 		usleep(200000);
	 		puts("No change");
	 	}
	}

	nfc_close(pnd);
	nfc_exit(context);

	return 0;
	_error:
	exit(EXIT_FAILURE);
}

