#!/bin/sh

#Takes a NormalCarte UID in argument and tries to open the Door is the UID is an authorized one.

if [ $# -eq 0 ]; then
	echo "No UID given in argument"
	exit 1
fi
if [[ $1 =~ ^[A-F0-9]{8}$ ]]
then
	:
else
	echo "Invalid UID"
	exit 1
fi

huid=`echo salthackens4242$1 | sha1sum - | cut -d ' ' -f 1`
found=0

IFS=","
while read f1 f2 f3 f4
do
	#printf "UID: %-8s || Name: %-20s || Date added: %-10s || Comment: %s\n" "$f1" "$f2" "$f3" "$f4"
	if [ $f1 = $huid ]
	then
		echo "Card $huid found for user \"$f2\" !"
                echo "1" > openme.txt
		found=1
		break
	fi
done < hackdoor_authorized_uids

if [ $found -eq 0 ]
then
	echo "Card not found; open denied"
        echo "1" > denied.txt
    echo "To allow card, enter this line in hackdoor_authorized_uids : "
    echo $huid,Nom,Date,Commentaire
	exit 1
fi

exit 0
