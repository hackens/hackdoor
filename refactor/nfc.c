/**
 * @file alert_uid.c
 * @brief Alerts when a card is detected and gives the uid
 * @args You get DISPERROR if argc>1
 * @state Return the uid after polling for max 20 secs
 * @todo  Decide what to do when we get we get an uid
 */

#include <stdlib.h>
#include <stdint.h>
#include <inttypes.h>
#include <signal.h>
#include <time.h>
#include <sys/time.h>
#include <unistd.h>
#include <nfc/nfc.h>

#define DISPERROR(_msg)            do{ fputs  ("" _msg "\n", stderr); }while(0);
//#define DISPVERBOSE(_msg)          do{ if(verbose) {fputs  ("" _msg "\n", stderr);       } }while(0);
#define DISPERRORARG(_msg,_args)   do{ fprintf(stderr, "" _msg "\n", _args); }while(0);
//#define DISPVERBOSEARG(_msg,_args) do{ if(verbose) {fprintf(stderr, "" _msg "\n", _args);} }while(0);

static nfc_device *pnd = NULL;
static nfc_context *context = NULL;
//static int verbose = 0;

#if 0
static void print_hex(const uint8_t *pbtData, const size_t szBytes)
{
  size_t szPos;
  for (szPos = 0; szPos < szBytes; szPos++) {
    printf("%02x", pbtData[szPos]);
  }
}
#endif

/* Tries to put the RFID module back in an usable state so we do not get an error next time we try to use it.
   This error is quite random and can still appears even with this cleanup. */
static void stop_polling(__attribute__((unused)) int sig)
{
	DISPERROR("SIGINT Received");
	if (pnd != NULL)
	{
		nfc_abort_command(pnd);
		nfc_close(pnd);
	}
	if(context != NULL)
	{
		nfc_exit(context);
	}
	fputs("\n", stderr);
	exit(EXIT_FAILURE);
}

/**
 * Wait for NFC card and return ID.
 * Return values:
 *   0: no card found
 *  between 1 and 2^32-1: card found; returns the card ID
 *  >=2^32: error (currently, only ~0 used)
 */
uint64_t get_nfc_id(void)
{
	nfc_target nt;
	uint64_t id = ~UINT64_C(0);

	const uint8_t uiPollNr = 1;
	const uint8_t uiPeriod = 1; /* We might need to ++ this if the card is too far */
	const nfc_modulation nmModulations[1] = /* Hoping it is not an oversimplification, but the daemom example does it too */
    { { .nmt = NMT_ISO14443A, .nbr = NBR_106 } };
	const size_t szModulations = 1;

	if (nfc_initiator_poll_target(pnd, nmModulations, szModulations, uiPollNr, uiPeriod, &nt) < 0)
	{
		//DISPERROR("No tag found");
        return UINT64_C(0);
	}

	if (nt.nm.nmt == nmModulations[0].nmt && nt.nm.nbr == nmModulations[0].nbr)
	{
		if((nt.nti.nai.szUidLen != 4) || (nt.nti.nai.abtAtqa[0] != 0x00) || (nt.nti.nai.abtAtqa[1] != 0x04) || (nt.nti.nai.btSak != 0x08) )
		{
			DISPERROR("Error: Not a NormaleCarte"); /* Actually: not a Mifare 1K card */
            return ~UINT64_C(0);
		}
		/* Assumes nt.nti.nai.szUidLen == 4 from above */
		id  = ((uint64_t)(nt.nti.nai.abtUid[3])) <<  0;
		id |= ((uint64_t)(nt.nti.nai.abtUid[2])) <<  8;
		id |= ((uint64_t)(nt.nti.nai.abtUid[1])) << 16;
		id |= ((uint64_t)(nt.nti.nai.abtUid[0])) << 24;
	} else
	{
		DISPERROR("Unsupported NFC type.");
        return ~UINT64_C(0);
	}
    return id;
}

int init_nfc() {
    signal(SIGINT, stop_polling);

	nfc_init(&context);	/* Initialize libnfc and set the nfc_context */
	if (context == NULL)
	{
		DISPERROR("Error: Unable to init libnfc (malloc)");
        return 0;
	}

	// Open, using the first available NFC device which can be in order of selection:
	//   - default device specified using environment variable or
	//   - first specified device in libnfc.conf (/etc/nfc) or
	//   - first specified device in device-configuration directory (/etc/nfc/devices.d) or
	//   - first auto-detected (if feature is not disabled in libnfc.conf) device
	pnd = nfc_open(context, NULL);
	if (pnd == NULL)
	{
		DISPERROR("Error: Unable to open NFC device.");
        return 0;
	}

	/* Set opened NFC device to initiator mode*/
	if (nfc_initiator_init(pnd) < 0)
	{
        DISPERROR("nfc_initiator_init() failed")  /* nfc_perror(pnd, "nfc_initiator_init"); */
        return 0;
	}
    return 1;
}

void close_nfc() {
	nfc_close(pnd);
	nfc_exit(context);
}
