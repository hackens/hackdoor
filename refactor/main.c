/**
 * @file alert_uid.c
 * @brief Alerts when a card is detected and gives the uid
 * @args You get DISPVERBOSE if argc>1
 * @state Return the uid after polling for max 20 secs
 * @todo  Decide what to do when we get we get an uid
 */

#include <stdlib.h>
#include <stdint.h>
#include <inttypes.h>
#include <signal.h>
#include <time.h>
#include <sys/time.h>
#include <unistd.h>
#include "nfc.h"
#include <nfc/nfc.h>

static int verbose = 0;

#define DISPVERBOSE(_msg)          do{ if(verbose) {fputs  ("" _msg "\n", stderr);       } }while(0);
#define DISPVERBOSEARG(_msg,_args) do{ if(verbose) {fprintf(stderr, "" _msg "\n", _args);} }while(0);

int main(int argc, __attribute__((unused)) const char *argv[])
{
 	if(argc>1)
 	{
 		verbose = 1;
 	}
    init_nfc();

	uint64_t id = UINT64_C(0);
    while(1)
	{
	 	uint64_t new_id;
	 	new_id = get_nfc_id();
	 	if(new_id > UINT64_C(0xFFFFFFFF))
	 	{
	 		puts("Failed to get ID");
	 		continue; /* An error happened. Let's try again. */
	 	}
	 	if(new_id != id)
	 	{
	 		int tryopen = 0;
	 		if(id == UINT64_C(0))
	 		{
	 			printf("New card found: %"PRIX64 "\n", new_id);
	 			tryopen = 1;
	 		} else if (new_id == UINT64_C(0))
	 		{
	 			printf("Card removed: %"PRIX64 "\n", new_id);
	 		} else
	 		{
	 			printf("New card found (but the previous one was not removed): %"PRIX64 "\n", new_id);
				tryopen = 1;
	 		}
	 		id = new_id;
            /*
	 		if(tryopen)
	 		{
	 			char command [100];
	 			snprintf(command, 24, "./auth.sh %"PRIX64, id);
	 			if(system(command))
	 			{
	 				DISPVERBOSE("Auth failed");
	 			}
	 		}
            */
	 	} else
	 	{
	 		//TODO: Add timeout for removal
	 		//TODO: sleep here a little? (but detection is already quite slow...)
	 		usleep(200000);
	 		puts("No change");
	 	}
	}

#if 0
	nfc_close(pnd);
	nfc_exit(context);
#endif
    close_nfc();

	return 0;
	_error:
	exit(EXIT_FAILURE);
}

