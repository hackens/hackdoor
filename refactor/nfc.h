#ifndef NFC_H
#define NFC_H

int init_nfc();
uint64_t get_nfc_id(void);
void close_nfc();

#endif

